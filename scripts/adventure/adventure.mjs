import * as handlers from './importer.mjs';

/**
 * Configuration of the adventure being exported/imported
 */
export const ADVENTURE = {
  // Basic information about the module and the adventure compendium it contains
  moduleName: 'ag-7cog',
  packName: 'ag-7cog-adventure',
  packId: 'ag-7cog.ag-7cog-adventure',
  adventureUuid:
    'Compendium.ag-7cog.ag-7cog-adventure.Adventure.x5BYtRQeZbvJ4SVv',
  adventureId: 'x5BYtRQeZbvJ4SVv',
  changelogId: '9AJG4pmMB3u1IzxT',

  // A CSS class to automatically apply to application windows which belong to this module
  cssClass: 'ag-7cog',

  description: `Героям в руки попадает зачарованная книга безумного волшебника Гилберта. По легенде, её страницы скрывают путь к месту, где спрятаны три кольца необычайной магической силы. Смогут ли герои пройти все испытания и заполучить один из ценных артефактов?`,

  // Define special Import Options with custom callback logic
  importOptions: {
    displayJournal: {
      label: 'Открыть текст приключения',
      default: true,
      handler: handlers.displayJournal,
      documentId: 'sHBD9SZpwbrAtc0t',
    },
    customizeJoin: {
      label: 'Преобразовать игровой мир',
      default: false,
      handler: handlers.customizeJoin,
      background: 'modules/ag-7cog/assets/art/adventure-cover.webp',
    },
  },
};
