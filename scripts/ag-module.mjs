import Ag7CogJournalSheet from './journal-sheet.mjs';
import {
  handleImportControls,
  onImport,
  renderAdventureImporter,
} from './adventure/importer.mjs';
import { ADVENTURE } from './adventure/adventure.mjs';

/* -------------------------------------------- */
/*  Hooks                                       */
/* -------------------------------------------- */

Hooks.on('init', () => {
  // const module = globalThis['ag-7cog'] = game.modules.get("ag-7cog");

  // Register Journal Sheet
  DocumentSheetConfig.registerSheet(
    JournalEntry,
    'ag-7cog',
    Ag7CogJournalSheet,
    {
      types: ['base'],
      label: '🧙 7 испытаний Гилберта',
      makeDefault: false,
    },
  );
});

Hooks.on('ready', async () => {
  const imported = !!game.settings.get('core', 'adventureImports')?.[
    ADVENTURE.adventureUuid
  ];
  if (!imported && game.user.isGM) {
    const pack = game.packs.get(ADVENTURE.packId);
    const adventure = await pack.getDocument(ADVENTURE.adventureId);
    adventure.sheet.render(true);
  }
});

/* -------------------------------------------- */
/*  Adventure Import					                  */
/* -------------------------------------------- */

Hooks.on('renderAdventureImporter', renderAdventureImporter);
Hooks.on('preImportAdventure', handleImportControls);
Hooks.on('importAdventure', onImport);
