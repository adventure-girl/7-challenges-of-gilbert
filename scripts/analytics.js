class ModuleAnalytics {
    static ANALYTICS_PREFIX = 'analytics';
    static SHARED_ID_KEY = 'ag-analytics-world-instance';
    static MODULE_ID;
    static POSTHOG_INITIALIZATION_PROMISE_KEY;
    static POSTHOG_INSTANCE_KEY; // Shared instance key

    static POSTHOG_CONFIG = {
        apiKey: 'phc_aIJkVRceCBA7ljLf96mSAHLpxDt2t4XutOhgK1PFEoi',
        host: 'https://us.i.posthog.com'
    };

    static init() {
        const scriptPath = import.meta.url;
        this.MODULE_ID = scriptPath.split('/modules/')[1].split('/')[0];

        const moduleTitle = game.modules.get(this.MODULE_ID)?.title || this.MODULE_ID;
        this.POSTHOG_INSTANCE_KEY = `postHog${this.POSTHOG_CONFIG.apiKey.slice(-32)}`;
        this.POSTHOG_INITIALIZATION_PROMISE_KEY = `postHogInitializationPromise${this.POSTHOG_CONFIG.apiKey.slice(-32)}`;

        const settingsName = game.i18n.language === 'ru' ?
            'Enable Analytics' :
            'Включить аналитику';
        const settingsHint = game.i18n.language === 'ru' ?
            'Allow anonymous module usage data collection (collects: Foundry version, system version, module version, country, errors and conflicts)' :
            'Разрешить сбор анонимных данных об использовании модуля (собираются: версия Foundry, версия системы, версия модуля, страна, ошибки и конфликты)';

        game.settings.register(this.MODULE_ID, this.getSettingId('enabled'), {
            name: settingsName,
            hint: settingsHint,
            scope: 'world',
            config: true,
            type: Boolean,
            default: true,
            namespace: moduleTitle
        });

        try {
            game.settings.register('core', this.SHARED_ID_KEY, {
                scope: 'world',
                config: false,
                type: String,
                default: ''
            });
        } catch (error) {
            console.error(`[${this.MODULE_ID}][Analytics] Error registering shared ID:`, error);
        }

        window.addEventListener('error', (event) => {
            if (!game.user.isGM) return;
            if (!event.filename?.includes(this.MODULE_ID)) return;
            this.captureEvent('module_error', {
                error: event.message,
                line: event.lineno,
                file: event.filename.split('/').pop()
            });
        });
    }

    static async ready() {
        if (!game.user.isGM) return;
        try {
            await this.getPostHogInstance();
            await this.onReady();
            await this.checkConflicts();
        } catch (error) {
            console.error(`[${this.MODULE_ID}][Analytics] Error in ready hook:`, error);
        }
    }

    static getSettingId(setting) {
        return `${this.MODULE_ID}-${this.ANALYTICS_PREFIX}-${setting}`;
    }

    static async captureEvent(eventName, properties = {}) {
        if (!game.settings.get(this.MODULE_ID, this.getSettingId('enabled'))) {
            console.log(`[${this.MODULE_ID}] Analytics disabled`);
            return;
        }

        try {
            const posthogInstance = await this.getPostHogInstance();
            const worldHash = await this.getWorldInstanceId();

            posthogInstance.capture(eventName, {
                ...properties,
                moduleId: this.MODULE_ID,
                distinct_id: worldHash,
                person: worldHash,
                event_time: new Date().toISOString()
            });
        } catch (error) {
            console.error(`[${this.MODULE_ID}] Failed to send analytics for ${eventName}:`, error);
        }
    }

    static async getPostHogInstance() {
        if (window[this.POSTHOG_INSTANCE_KEY]) {
            // Return the shared instance
            return window[this.POSTHOG_INSTANCE_KEY];
        }

        if (window[this.POSTHOG_INITIALIZATION_PROMISE_KEY]) {
            // Return Promise in case PostHog is being initialized right now
            // by another module from the same author
            return window[this.POSTHOG_INITIALIZATION_PROMISE_KEY];
        }

        window[this.POSTHOG_INITIALIZATION_PROMISE_KEY] = new Promise((resolve, reject) => {
            try {
                !function(t,e){var o,n,p,r;e.__SV||(window.posthog=e,e._i=[],e.init=function(i,s,a){function g(t,e){var o=e.split(".");2==o.length&&(t=t[o[0]],e=o[1]),t[e]=function(){t.push([e].concat(Array.prototype.slice.call(arguments,0)))}}(p=t.createElement("script")).type="text/javascript",p.crossOrigin="anonymous",p.async=!0,p.src=s.api_host.replace(".i.posthog.com","-assets.i.posthog.com")+"/static/array.js",(r=t.getElementsByTagName("script")[0]).parentNode.insertBefore(p,r);var u=e;for(void 0!==a?u=e[a]=[]:a="posthog",u.people=u.people||[],u.toString=function(t){var e="posthog";return"posthog"!==a&&(e+="."+a),t||(e+=" (stub)"),e},u.people.toString=function(){return u.toString(1)+".people (stub)"},o="init capture register register_once register_for_session unregister unregister_for_session getFeatureFlag getFeatureFlagPayload isFeatureEnabled reloadFeatureFlags updateEarlyAccessFeatureEnrollment getEarlyAccessFeatures on onFeatureFlags onSessionId getSurveys getActiveMatchingSurveys renderSurvey canRenderSurvey getNextSurveyStep identify setPersonProperties group resetGroups setPersonPropertiesForFlags resetPersonPropertiesForFlags setGroupPropertiesForFlags resetGroupPropertiesForFlags reset get_distinct_id getGroups get_session_id get_session_replay_url alias set_config startSessionRecording stopSessionRecording sessionRecordingStarted captureException loadToolbar get_property getSessionProperty createPersonProfile opt_in_capturing opt_out_capturing has_opted_in_capturing has_opted_out_capturing clear_opt_in_out_capturing debug".split(" "),n=0;n<o.length;n++)g(u,o[n]);e._i.push([i,s,a])},e.__SV=1)}(document,window.posthog||[]);

                let timerId;
                posthog.init(this.POSTHOG_CONFIG.apiKey, {
                    api_host: this.POSTHOG_CONFIG.host,
                    autocapture: false,
                    capture_pageview: false,
                    disable_session_recording: true,
                    property_blacklist: [
                        '$current_url', '$host', '$pathname', '$browser',
                        '$browser_version', '$device_type', '$os', '$os_version',
                        '$geoip_city', '$geoip_city_name', '$geoip_continent',
                        '$geoip_latitude', '$geoip_longitude', '$geoip_postal_code',
                        '$geoip_region', '$geoip_region_name', '$geoip_timezone'
                    ],
                    property_allowlist: ['$geoip_country_name', '$geoip_country_code'],
                    loaded: (posthogInstance) => {
                        clearTimeout(timerId); // Cancel the timeout
                        window[this.POSTHOG_INSTANCE_KEY] = posthogInstance;
                        resolve(posthogInstance);
                    },
                });

                timerId = setTimeout(() => {
                    if (!window[this.POSTHOG_INSTANCE_KEY]) {
                        reject(new Error('PostHog initialization timed out'));
                    }
                }, 10000);
            } catch (error) {
                reject(error);
            }
        });

        return window[this.POSTHOG_INITIALIZATION_PROMISE_KEY];
    }

    static async getWorldInstanceId() {
        try {
            let instanceId = game.settings.get('core', this.SHARED_ID_KEY);

            if (!instanceId) {
                instanceId = this.hashString(game.world.id + Date.now().toString());
                await game.settings.set('core', this.SHARED_ID_KEY, instanceId);
            }
            return instanceId;
        } catch (error) {
            console.error(`[${this.MODULE_ID}][Analytics] Error getting world instance ID:`, error);
            return this.hashString(game.world.id);
        }
    }

    static hashString(str) {
        let hash = 0;
        for (let i = 0; i < str.length; i++) {
            const char = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + char;
            hash = hash & hash;
        }
        return 'w' + Math.abs(hash).toString(36);
    }

    static async checkConflicts() {
        const conflicts = [];

        game.modules.forEach(otherModule => {
            if (otherModule.id === this.MODULE_ID) return;

            if (otherModule.relationships?.systems) {
                const systemReq = otherModule.relationships.systems.find(s => s.id === game.system.id);
                if (systemReq && !isVersionCompatible(systemReq.compatibility?.minimum, game.system.version)) {
                    conflicts.push({
                        type: 'system_version',
                        module: otherModule.id,
                        details: `Requires system ${game.system.id} >= ${systemReq.compatibility.minimum}`
                    });
                }
            }
        });

        if (conflicts.length > 0) {
            this.captureEvent('module_conflicts', { conflicts });
        }
    }

    static async onReady() {
        if (!game.settings.get(this.MODULE_ID, this.getSettingId('enabled'))) return;

        const module = game.modules.get(this.MODULE_ID);
        if (!module) return;

        const worldHash = await this.getWorldInstanceId();
        const time =  new Date().toISOString();
        const data = {
            moduleId: this.MODULE_ID,
            moduleTitle: module.title,
            moduleVersion: module.version,
            foundryVersion: game.version,
            systemId: game.system.id,
            systemVersion: game.system.version,
            isActive: module.active,
            interfaceLanguage: game.i18n.language,
            distinct_id: worldHash,
            person: worldHash,
            event_time: time
        };

        await this.captureEvent('module_loaded', data);
    }
}

function isVersionCompatible(required, current) {
    if (!required || !current) return true;
    const [reqMajor, reqMinor = 0] = required.split('.').map(Number);
    const [curMajor, curMinor = 0] = current.split('.').map(Number);
    return curMajor > reqMajor || (curMajor === reqMajor && curMinor >= reqMinor);
}

Hooks.once('init', () => {
    ModuleAnalytics.init();
});

Hooks.once('ready', () => {
    ModuleAnalytics.ready();
});